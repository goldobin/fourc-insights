name := "fourc-insights"

version := "0.1"

scalaVersion := "2.12.3"

Defaults.itSettings

lazy val root = project in file(".") configs IntegrationTest

val akkaVersion = "2.4.19"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % "10.0.10",

  "org.json4s" %% "json4s-ext" % "3.5.3",
  "org.json4s" %% "json4s-jackson" % "3.5.3",
  "de.heikoseeberger" %% "akka-http-json4s" % "1.17.0",

  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "com.typesafe.akka" % "akka-testkit_2.12" % akkaVersion % "test",

  "org.scalatest" %% "scalatest" % "3.0.4" % "it,test"
)

mainClass in run := Some("Boot")

