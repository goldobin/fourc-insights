import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class DiffServiceSpec
    extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender
    with FlatSpecLike
    with Matchers
    with BeforeAndAfterAll {

  import DiffService._

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "A DiffService" should "write side and respond" in {

    val storageProb = TestProbe()

    val service = system.actorOf(DiffService.props(storageProb.ref))

    service ! SaveSide(10, DiffSideType.Left, ByteString("Test Data"))

    storageProb expectMsg DiffSideStorage.WriteSide(
      id = 10,
      side = DiffSideType.Left,
      data = ByteString("Test Data")
    )
    storageProb reply DiffSideStorage.SideWritten(10, DiffSideType.Left)

    expectMsg(SideSaved(10, DiffSideType.Left))
  }

  it should "not calculate diff if there is no one of the parts" in {
    val storageProb = TestProbe()

    val service = system.actorOf(DiffService.props(storageProb.ref))

    service ! GetDiff(20)

    storageProb expectMsg DiffSideStorage.ReadSides(20)
    storageProb reply DiffSideStorage.Sides(
      id = 20,
      left = Some(ByteString("Test content")),
      right = None
    )

    expectMsg(Diff(id = 20, description = None))
  }

  it should "calculate diff" in {

    val storageProb = TestProbe()

    val service = system.actorOf(DiffService.props(storageProb.ref))

    service ! GetDiff(30)

    storageProb expectMsg DiffSideStorage.ReadSides(30)
    storageProb reply DiffSideStorage.Sides(
      id = 30,
      left = Some(ByteString("Test content")),
      right = Some(ByteString("Test content"))
    )

    expectMsg(Diff(id = 30, description = Some(DiffDescription(DiffResultType.Equals))))
  }
}
