import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.ByteString
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class DiffSideStorageSpec
    extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender
    with FlatSpecLike
    with Matchers
    with BeforeAndAfterAll {

  import DiffSideStorage._

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "A DiffSideStorage" should "return empty result while reading unknown diff id" in {
    val storage = system.actorOf(props)

    storage ! ReadSides(1000)
    expectMsg(Sides(1000, None, None))
  }

  it should "write and rewrite left side" in {

    val storage = system.actorOf(props)

    storage ! WriteSide(
      10,
      DiffSideType.Left,
      ByteString("Left Side Test Data 1")
    )
    expectMsg(SideWritten(10, DiffSideType.Left))

    storage ! ReadSides(10)
    expectMsg(Sides(10, Some(ByteString("Left Side Test Data 1")), None))

    storage ! WriteSide(
      10,
      DiffSideType.Left,
      ByteString("Left Side Test Data 2")
    )
    expectMsg(SideWritten(10, DiffSideType.Left))

    storage ! ReadSides(10)
    expectMsg(Sides(10, Some(ByteString("Left Side Test Data 2")), None))
  }

  it should "write and rewrite right side" in {

    val storage = system.actorOf(props)

    storage ! WriteSide(
      20,
      DiffSideType.Left,
      ByteString("Right Side Test Data 1")
    )
    expectMsg(SideWritten(20, DiffSideType.Left))

    storage ! ReadSides(20)
    expectMsg(Sides(20, Some(ByteString("Right Side Test Data 1")), None))

    storage ! WriteSide(
      20,
      DiffSideType.Left,
      ByteString("Right Side Test Data 2")
    )
    expectMsg(SideWritten(20, DiffSideType.Left))

    storage ! ReadSides(20)
    expectMsg(Sides(20, Some(ByteString("Right Side Test Data 2")), None))
  }

  it should "write both sides and combine them on read" in {
    val storage = system.actorOf(props)

    storage ! WriteSide(
      30,
      DiffSideType.Left,
      ByteString("Left Side Test Data")
    )
    expectMsg(SideWritten(30, DiffSideType.Left))

    storage ! WriteSide(
      30,
      DiffSideType.Right,
      ByteString("Right Side Test Data")
    )
    expectMsg(SideWritten(30, DiffSideType.Right))

    storage ! ReadSides(30)

    expectMsg(
      Sides(
        30,
        Some(ByteString("Left Side Test Data")),
        Some(ByteString("Right Side Test Data"))
      )
    )
  }
}
