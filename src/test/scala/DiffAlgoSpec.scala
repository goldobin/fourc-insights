import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks

import DiffResultType._

class DiffAlgoSpec extends FlatSpec with Matchers with TableDrivenPropertyChecks {

  "A DiffAlgo" should "compare contents and provide diff description" in {
    val cases = Table(
      ("left", "right", "result"),
      ("", "", DiffDescription(Equals)),
      ("Hello World!", "Hello World!", DiffDescription(Equals)),
      ("Hello World!", "Hello, World!", DiffDescription(SizeDoNotMatch)),
      ("hello, World!",
       "Hello, World!",
       DiffDescription(
         ContentDoNotMatch,
         Some(List(DiffEntry(0, 1)))
       )),
      ("HELLO, WORLD!",
       "Hello, World!",
       DiffDescription(
         ContentDoNotMatch,
         Some(List(DiffEntry(1, 4), DiffEntry(8, 4)))
       ))
    )

    forAll(cases) { (left, right, result) =>
      DiffAlgo.diff(left, right) shouldBe result
    }
  }
}
