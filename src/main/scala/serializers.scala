import java.nio.charset.StandardCharsets
import java.util.Base64

import akka.util.ByteString
import org.json4s.{CustomSerializer, JValue}
import org.json4s.JsonAST.JString
import org.json4s.ext.EnumNameSerializer

/**
  * Marshallers for Base64Serializer
  */
object Base64Marshallers {
  def unmarshall: PartialFunction[JValue, ByteString] = {
    case JString(s) =>
      ByteString.fromArray(Base64.getDecoder.decode(s))
  }

  def marshall: PartialFunction[Any, JValue] = {
    case x: ByteString =>
      val bytes: Array[Byte] = new Array[Byte](x.size)
      x.copyToArray(bytes)

      JString(
        new String(
          Base64.getEncoder.encode(bytes),
          StandardCharsets.UTF_8
        )
      )
  }
}

import Base64Marshallers._

/**
  * A custom Json4s serializer to convert base 64 encoded string from/to ByteString
  */
object Base64Serializer extends CustomSerializer[ByteString](_ => (unmarshall, marshall))

/**
  * A custom Json4s serializer to convert string from/to DiffResultType enumeration values
  */
object DiffResultTypeSerializer extends EnumNameSerializer(DiffResultType)