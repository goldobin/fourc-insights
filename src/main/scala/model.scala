import akka.util.ByteString

object DiffResultType extends Enumeration {
  type DiffResultType = Value
  val SizeDoNotMatch, Equals, ContentDoNotMatch = Value
}

object DiffSideType extends Enumeration {
  type DiffSideType = Value
  val Left, Right = Value
}

import DiffResultType._

case class DiffSideData(data: ByteString)

case class DiffEntry(offset: Int, length: Int)

case class DiffDescription(
    diffResultType: DiffResultType,
    diffs: Option[List[DiffEntry]] = None
)
