import DiffSideType.DiffSideType
import akka.actor.{Actor, Props}
import akka.util.ByteString

import scala.collection.mutable

/**
  * A DiffSideStorage messages
  */
object DiffSideStorage {

  // Write request
  case class WriteSide(id: Int, side: DiffSideType, data: ByteString)

  // Write acknowledgement (ack)
  case class SideWritten(id: Int, side: DiffSideType)

  // Read request
  case class ReadSides(id: Int)

  // Read response
  case class Sides(id: Int, left: Option[ByteString], right: Option[ByteString])

  def props: Props = Props[DiffSideStorage]()
}

/**
  * Stores diff sides in memory by diff id
  */
class DiffSideStorage extends Actor {

  import DiffSideStorage._

  private val sides = Vector[mutable.Map[Int, ByteString]](
    mutable.Map(),
    mutable.Map()
  )

  private def sideIndex(diffSideType: DiffSideType): Int = {
    if (diffSideType == DiffSideType.Left) 0 else 1
  }

  override def receive: Receive = {
    case ReadSides(id) =>
      val left: Option[ByteString] = sides(sideIndex(DiffSideType.Left)).get(id)
      val right: Option[ByteString] = sides(sideIndex(DiffSideType.Right)).get(id)

      sender() ! Sides(id, left, right)

    case WriteSide(id, side, data) =>
      sides(sideIndex(side)) += id -> data

      sender() ! SideWritten(id, side)
  }
}
