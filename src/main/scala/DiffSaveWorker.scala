import DiffSideType.DiffSideType
import akka.actor.{Actor, ActorRef, Cancellable, PoisonPill, Props}

import scala.concurrent.duration._

object DiffSaveWorker {
  private case object Timedout
  def props(storage: ActorRef): Props = Props(new DiffSaveWorker(storage))
}

/**
  * The main responsibility of this actor is to call storage to write record and
  * respond back when write is finished
  *
  * @param storage - a diff side storage
  */
class DiffSaveWorker(storage: ActorRef) extends Actor {
  import DiffSaveWorker.Timedout

  override def receive: Receive = inital

  def inital: Receive = {
    case DiffService.SaveSide(id, side, data) =>
      storage ! DiffSideStorage.WriteSide(id, side, data)
      val timeoutCancelable =
        context.system.scheduler.scheduleOnce(5.seconds, self, Timedout)(context.dispatcher)
      context become waitForAck(id, side, sender(), timeoutCancelable)
  }

  def waitForAck(
      id: Int,
      side: DiffSideType,
      respondTo: ActorRef,
      timeoutCancelable: Cancellable
  ): Receive = {
    case DiffSideStorage.SideWritten(receivedId, receivedSide) =>
      require(receivedId == id)
      require(receivedSide == receivedSide)

      timeoutCancelable.cancel()
      respondTo ! DiffService.SideSaved(id, side)

      context stop self

    case Timedout =>
      context stop self
  }
}
