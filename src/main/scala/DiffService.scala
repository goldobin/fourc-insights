import DiffSideType._
import akka.actor.{Actor, ActorRef, Props, SupervisorStrategy}
import akka.util.ByteString

/**
  * A DiffService messages
  */
object DiffService {
  // Save request
  case class SaveSide(
      id: Int,
      side: DiffSideType,
      data: ByteString
  )

  // Save request ack
  case class SideSaved(id: Int, side: DiffSideType)

  // Get dif request
  case class GetDiff(id: Int)

  // Get diff response
  case class Diff(
      id: Int,
      description: Option[DiffDescription] = None
  )

  def props(storage: ActorRef) = Props(new DiffService(storage))
}

/**
  * A supervisor actor that creates particular worker actors and forward them service tasks
  *
  * This actor will not restart its workers in case of failure. The caller should maintain a timeout
  * while calling to this service as soon as this service may not respond in case of failure.
  *
  * @param storage - a storage actor
  */
class DiffService(storage: ActorRef) extends Actor {

  import DiffService._

  // If worker fail do not restart it.
  // The caller should maintain a timeout while calling to this service
  // as soon as this service may not respond in case of error
  override def supervisorStrategy: SupervisorStrategy = SupervisorStrategy.stoppingStrategy

  override def receive: Receive = {
    case m: SaveSide =>
      val worker = context.actorOf(DiffSaveWorker.props(storage))
      worker forward m

    case m: GetDiff =>
      val worker = context.actorOf(DiffCalcWorker.props(storage))
      worker forward m
  }
}
