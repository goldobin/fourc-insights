import akka.pattern.ask
import DiffSideType.DiffSideType
import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.{ByteString, Timeout}
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s.{DefaultFormats, Formats, Serialization, jackson}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration._

case class DiffRoute(diffService: ActorRef) {

  import Json4sSupport._
  import StatusCodes._


  implicit val executionContext: ExecutionContextExecutor =
    scala.concurrent.ExecutionContext.global

  private implicit val serialization: Serialization = jackson.Serialization
  private implicit val formats: Formats =
    DefaultFormats + Base64Serializer + DiffResultTypeSerializer

  private implicit val timeout: Timeout = Timeout(5.seconds)

  private def putSide(id: Int, side: DiffSideType, data: ByteString): Future[Unit] = {
    diffService ? DiffService.SaveSide(id, side, data)
  }.map(_ => Unit)

  private def getDiff(id: Int): Future[Option[DiffDescription]] = {
    diffService ? DiffService.GetDiff(id)
  }.map {
    case DiffService.Diff(_, description) => description
  }

  val route: Route = pathPrefix("v1") {
    pathPrefix("diff" / IntNumber) { (id: Int) =>
      path("left") {
        put {
          entity(as[DiffSideData]) { body =>
            onSuccess(putSide(id, DiffSideType.Left, body.data)) {
              complete(Created, List(Location(s"/v1/diff/$id/left")))
            }
          }
        }
      } ~
      path("right") {
        put {
          entity(as[DiffSideData]) { body =>
            onSuccess(putSide(id, DiffSideType.Right, body.data)) {
              complete(Created, List(Location(s"/v1/diff/$id/right")))
            }
          }
        }
      } ~
      pathEnd {
        get {
          rejectEmptyResponse {
            complete {
              getDiff(id)
            }
          }
        }
      }
    }
  }
}
