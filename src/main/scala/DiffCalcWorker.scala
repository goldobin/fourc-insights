import akka.actor.{Actor, ActorRef, Cancellable, PoisonPill, Props}

import scala.concurrent.duration._

object DiffCalcWorker {
  private case object Timedout
  def props(storage: ActorRef): Props = Props(new DiffCalcWorker(storage))
}

/**
  * The main responsibility of this actor is to call storage for the diff parts, calculate difference and
  * respond back with the diff result
  *
  * @param storage - a diff side storage
  */
class DiffCalcWorker(storage: ActorRef) extends Actor {
  import DiffCalcWorker.Timedout

  override def receive: Receive = initial

  def initial: Receive = {
    case DiffService.GetDiff(id) =>
      storage ! DiffSideStorage.ReadSides(id)

      val timeoutCancellable = context.system.scheduler.scheduleOnce(5.seconds, self, Timedout)(context.dispatcher)
      context become waitForData(id, sender(), timeoutCancellable)
  }

  def waitForData(id: Int, respondTo: ActorRef, timeoutCancellable: Cancellable): Receive = {
    case DiffSideStorage.Sides(receivedId, left, right) =>
      require(receivedId == id)

      timeoutCancellable.cancel()

      val data: Option[DiffDescription] = (left zip right).headOption map {
        case (l, r) => DiffAlgo.diff(l, r)
      }

      respondTo ! DiffService.Diff(id, data)
      context stop self

    case Timedout =>
      context stop self
  }
}
