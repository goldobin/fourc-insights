import DiffResultType.{ContentDoNotMatch, Equals}

import scala.annotation.tailrec

/**
  * An implementation of sequences difference algorithm
  */
object DiffAlgo {

  /**
    * Finds and describes difference between two sequences.
    *
    * If sequences are match returns `Equals`
    *
    * If sequences are different in size returns `SizeDoNotMatch`
    *
    * If sequences are the same size but different content returns `ContentDoNotMatch` and a sequence of
    * offset and length pairs describing the difference
    *
    * @param left - a left sequence
    * @param right - a right sequence
    * @tparam T - the type of entry
    * @return
    */
  def diff[T](left: Seq[T], right: Seq[T]): DiffDescription = {

    if (left.size != right.size) {
      DiffDescription(DiffResultType.SizeDoNotMatch)
    } else {

      sealed trait Section
      case object MatchingSection extends Section
      case class NotMatchingSection(diffEntry: DiffEntry) extends Section

      @tailrec
      def iterate(
          index: Int,
          left: Seq[T],
          right: Seq[T],
          acc: List[Section]
      ): List[Section] = {

        if (left == Nil) {
          acc
        } else {
          val l +: leftTail = left
          val r +: rightTail = right

          val prevSection = if (acc.isEmpty) MatchingSection else acc.head

          val nextAcc: List[Section] = {
            if (l == r) {
              prevSection match {
                case MatchingSection =>
                  acc
                case NotMatchingSection(DiffEntry(_, _)) =>
                  MatchingSection :: acc
              }
            } else {
              prevSection match {
                case MatchingSection =>
                  NotMatchingSection(DiffEntry(index, 1)) :: acc

                case NotMatchingSection(DiffEntry(offset, length)) =>
                  NotMatchingSection(DiffEntry(offset, length + 1)) :: acc.tail
              }
            }
          }

          iterate(index + 1, leftTail, rightTail, nextAcc)
        }
      }

      val sections: List[Section] = iterate(0, left, right, List())

      val entries = sections collect {
        case NotMatchingSection(entry) => entry
      }

      if (entries.isEmpty) {
        DiffDescription(Equals)
      } else {
        DiffDescription(ContentDoNotMatch, Some(entries.reverse))
      }
    }
  }
}
