import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, Materializer}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContextExecutor
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

/**
  * The main class
  */
object Boot extends App with StrictLogging {

  // Define some implicits
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val materializer: Materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor =
    scala.concurrent.ExecutionContext.global

  // Instantiate actors
  private val storage = actorSystem.actorOf(DiffSideStorage.props, "storage")
  private val service = actorSystem.actorOf(DiffService.props(storage), "service")

  val listenHost = "localhost"
  val listenPort = 8080
  val route: Route = DiffRoute(service).route

  // Run the server
  val bindFuture = Http().bindAndHandle(route, listenHost, listenPort)

  bindFuture onComplete {
    case Success(binding) =>
      logger.info(s"The HTTP server is listening at ${binding.localAddress}")

    case Failure(NonFatal(e)) =>
      logger.error("Can't start the server", e)
      // Terminate an actor system and stop this application with non zero return code
      actorSystem.terminate().onComplete(_ => System.exit(1))
  }
}
