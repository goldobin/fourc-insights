import java.nio.charset.StandardCharsets
import java.util.Base64

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{LinkParams, LinkValue}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.Random
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonDSL._

class DiffServiceIntegrationSpec
    extends FlatSpec
    with Matchers
    with GivenWhenThen
    with TableDrivenPropertyChecks {

  private val baseUrl = "http://localhost:8080/v1"
  private val defaultTimeout = 5.seconds

  private implicit val system: ActorSystem = ActorSystem()
  private implicit val materializer: ActorMaterializer = ActorMaterializer()

  import HttpMethods._
  import MediaTypes._
  import StatusCodes._

  "A Diff Service" should "should return 404 when unknown diff requested" in {

    Given("GET request for not existing diff")

    val id = randomDiffId()

    val request = HttpRequest(
      method = GET,
      uri = s"$baseUrl/diff/$id"
    )

    When("request performed")
    val response: HttpResponse = awaitResult(Http().singleRequest(request))

    Then("response status should be '404 Not Found'")
    response.status shouldBe NotFound
  }

  private val Sides = Table(
    ("id", "side", "invalid_json"),
    (101, "left", """{}"""),
    (101, "right", """{}"""),
    (102, "left", """{"data": null}"""),
    (102, "right", """{"data": null}"""),
    (103, "left", """{"data": "++//"}"""),
    (103, "right", """{"data": "++//"}""")
  )

  forAll(Sides) { (id, side, invalidJson) =>
    it should s"not consume bad JSON in PUT request for the $side (json: $invalidJson)" in {

      Given("PUT diff request with invalid JSON")

      val request = HttpRequest(
        method = PUT,
        uri = s"$baseUrl/diff/$id/$side",
        entity = HttpEntity(
          `application/json`,
          s"""{"data": "$invalidJson"}"""
        )
      )

      When("request performed")
      val response: HttpResponse = awaitResult(Http().singleRequest(request))

      Then("response have status '400 Bad Request'")
      response.status shouldBe BadRequest
    }
  }

  private val SideData = Table(
    ("side", "data"),
    ("left", "SGVsbG8sIFdvcmxkIQ=="),
    ("right", "SGVsbG8sIFdvcmxkIQ==")
  )

  forAll(SideData) { (side, data) =>
    it should s"consume PUT request for the $side part of the diff" in {

      Given(s"PUT diff request for $side")

      val id = 151

      val request = HttpRequest(
        method = PUT,
        uri = s"$baseUrl/diff/$id/$side",
        entity = HttpEntity(
          `application/json`,
          s"""{"data": "$data"}"""
        )
      )

      When("request performed")
      val response: HttpResponse = awaitResult(Http().singleRequest(request))

      Then("response have status '201 Created'")
      response.status shouldBe Created

      And("contain link to diff")
      response.headers contains headers.Link(
        LinkValue("/v1/diff/1", LinkParams.rel("diff")))
    }
  }

  private val DiffData = {
    import org.json4s.JsonDSL._

    Table(
      ("id", "left", "right", "result_type", "diff_description"),
      (200, "Hello, World!", "Hello, World!", "Equals", None),
      (201, "Hello, World!", "Hello world?", "SizeDoNotMatch", None),
      (202,
       "Hello, World!",
       "HELLO, WORLD!",
       "ContentDoNotMatch",
       Some(
         JArray(
           List(
             ("offset" -> 1) ~ ("length" -> 4),
             ("offset" -> 8) ~ ("length" -> 4)
           ))))
    )
  }

  forAll(DiffData) {
    (id, left, right, resultType: String, diffDescription: Option[JValue]) =>
      it should s"consume PUT request for both sides and produce diff with result type '$resultType'" in {

        Given(s"PUT request for the left side")

        val leftContent = left.encodeBase64

        val putLeftRequest = HttpRequest(
          method = PUT,
          uri = s"$baseUrl/diff/$id/left",
          entity = HttpEntity(
            `application/json`,
            s"""{"data": "$leftContent"}"""
          )
        )

        And(s"PUT request for the right side")

        val rightContent = right.encodeBase64

        val putRightRequest = HttpRequest(
          method = PUT,
          uri = s"$baseUrl/diff/$id/right",
          entity = HttpEntity(
            `application/json`,
            s"""{"data": "$rightContent"}"""
          )
        )

        And(s"GET request for the diff")

        val getDiffRequest = HttpRequest(
          method = GET,
          uri = s"$baseUrl/diff/$id"
        )

        When("requests performed for parts")
        awaitResult(Http().singleRequest(putLeftRequest))
        awaitResult(Http().singleRequest(putRightRequest))

        And("get diff request performed")

        val response = awaitResult(Http().singleRequest(getDiffRequest))

        Then("response have status '200 OK'")
        response.status shouldBe OK

        And("contains description of diff")

        val expectedJson: JValue = diffDescription match {
          case None =>
            "diffResultType" -> resultType
          case Some(description) =>
            ("diffResultType" -> resultType) ~ ("diffs" -> description)
        }

        val entity = awaitHttpEntity(response.entity)
        val json: JValue = parseJson(entity.data)

        pretty(render(json)) shouldBe pretty(render(expectedJson))
      }
  }

  it should s"pass main flow" in {
    val id = 1

    {
      Given(s"PUT request for the left side")

      val leftContent = "AAAAAA=="
      val putLeftRequest = HttpRequest(
        method = PUT,
        uri = s"$baseUrl/diff/$id/left",
        entity = HttpEntity(
          `application/json`,
          s"""{"data": "$leftContent"}"""
        )
      )

      And(s"PUT request for the right side with equal data")

      val rightContent = "AAAAAA=="

      val putRightRequest1 = HttpRequest(
        method = PUT,
        uri = s"$baseUrl/diff/$id/right",
        entity = HttpEntity(
          `application/json`,
          s"""{"data": "$rightContent"}"""
        )
      )

      And(s"GET request for the diff")

      val getDiffRequest = HttpRequest(
        method = GET,
        uri = s"$baseUrl/diff/$id"
      )

      When("all requests performed")
      val putLeftResponse = awaitResult(Http().singleRequest(putLeftRequest))
      val putRightResponse = awaitResult(Http().singleRequest(putRightRequest1))
      val getDiffResponse = awaitResult(Http().singleRequest(getDiffRequest))

      Then("responses should have popper statuses")
      putLeftResponse.status shouldBe Created
      putRightResponse.status shouldBe Created
      getDiffResponse.status shouldBe OK

      And("diff has type 'Equals'")

      val json = extractJson(getDiffResponse)

      pretty(render(json)) shouldBe pretty(render("diffResultType" -> "Equals"))
    }

    {

      Given(
        "PUT request for the right side with the same size but different content")

      val rightContent = "AQABAQ=="

      val putRightRequest = HttpRequest(
        method = PUT,
        uri = s"$baseUrl/diff/$id/right",
        entity = HttpEntity(
          `application/json`,
          s"""{"data": "$rightContent"}"""
        )
      )

      And(s"GET request for the diff")

      val getDiffRequest2 = HttpRequest(
        method = GET,
        uri = s"$baseUrl/diff/$id"
      )

      When("PUT right requests performed")
      val putRightResponse = awaitResult(Http().singleRequest(putRightRequest))

      And("GET diff request performed")
      val getDiffResponse = awaitResult(Http().singleRequest(getDiffRequest2))

      Then("responses should have popper statuses")
      putRightResponse.status shouldBe Created
      getDiffResponse.status shouldBe OK

      And("diff has type 'ContentDoNotMatch' and contain description of differences")
      val json2 = extractJson(getDiffResponse)

      pretty(render(json2)) shouldBe pretty(render(
          ("diffResultType" -> "ContentDoNotMatch") ~
            ("diffs" -> Seq(
              ("offset" -> 0) ~ ("length" -> 1),
              ("offset" -> 2) ~ ("length" -> 2)
            ))
      ))
    }

    {
      Given("PUT request for the right side with data having different size")

      val rightContent = "AAA="

      val putRightRequest = HttpRequest(
        method = PUT,
        uri = s"$baseUrl/diff/$id/right",
        entity = HttpEntity(
          `application/json`,
          s"""{"data": "$rightContent"}"""
        )
      )

      And(s"GET request for the diff")

      val getDiffRequest = HttpRequest(
        method = GET,
        uri = s"$baseUrl/diff/$id"
      )

      When("PUT right requests performed")
      val putRightResponse = awaitResult(Http().singleRequest(putRightRequest))

      And("GET diff request performed")
      val getDiffResponse = awaitResult(Http().singleRequest(getDiffRequest))

      Then("responses should have popper statuses")
      putRightResponse.status shouldBe Created
      getDiffResponse.status shouldBe OK

      And("diff has type 'SizeDoNotMatch'")
      val json3 = extractJson(getDiffResponse)

      pretty(render(json3)) shouldBe pretty(
        render("diffResultType" -> "SizeDoNotMatch"))
    }
  }

  private def randomDiffId(): Int = {
    Int.MaxValue - 10000 + Random.nextInt(10000)
  }

  private def awaitResult[T](f: Future[T]): T = {
    Await.result(f, defaultTimeout)
  }

  private def awaitHttpEntity(entity: HttpEntity): HttpEntity.Strict = {
    awaitResult(entity.toStrict(1.second))
  }

  private def parseJson(raw: ByteString) = {
    import org.json4s._
    import org.json4s.jackson.JsonMethods._

    parse(raw.decodeString(StandardCharsets.UTF_8))
  }

  private def extractJson(response: HttpResponse) = {
    parseJson(awaitHttpEntity(response.entity).data)
  }

  implicit class Base64Ops(s: String) {
    def encodeBase64: String = {
      new String(Base64.getEncoder.encode(s.getBytes(StandardCharsets.UTF_8)),
                 StandardCharsets.UTF_8)
    }

    def decodeBase64: String = {
      new String(Base64.getDecoder.decode(s), StandardCharsets.UTF_8)
    }
  }

}
