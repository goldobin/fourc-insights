# 4C Test Assignment

## Description

Implementation of a simple RESTfull web-service to compare and provide differences description of the pair of 
binary data sequences

### Build

```sh
sbt compile test:compile it:compile test
```

### Run the server 

Run the server at `http://localhost:8080`

```sh
sbt run
```

Run integration tests against running server. Integration tests will use 
the default `http://localhost:8080` base URL

```sh
sbt it:test 
```

## Requests examples

PUT left

```
PUT http://localhost:8080/v1/diff/1/left
Content-Type: application/json
---
{
  "data": "AAAAAA=="
}
```

PUT right

```
PUT http://localhost:8080/v1/diff/1/right
Content-Type: application/json
---
{
  "data": "AAAAAA=="
}
```

GET diff

```
GET http://localhost:8080/v1/diff/1
```
